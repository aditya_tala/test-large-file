FROM maven:3.5.0-jdk-8

RUN apt-get update

WORKDIR /test-large-file

ADD ./target/notification-service-1.0-SNAPSHOT-jar-with-dependencies.jar /notifysvc/target/notification-service-1.0-SNAPSHOT-jar-with-dependencies.jar
ADD ./customer-details.txt /notifysvc/customer-details.txt

EXPOSE 2345
ENTRYPOINT ["/usr/lib/jvm/java-8-openjdk-amd64/bin/java", "-jar", "target/notification-service-1.0-SNAPSHOT-jar-with-dependencies.jar"]


